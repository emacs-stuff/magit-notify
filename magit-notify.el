;; Copyright (C) 2017 vindarel

;; Author:   Vindarel <ehvince@maiz.org>
;; URL:
;; Keywords: magit, notifications, desktop
;; Version: 0.0.1
;; Package-Requires: ((magit "0") (projectile "0"))
;; Summary: Un-missable desktop magit notifications.

;;; Commentary:

;; This minor mode allows to show desktop magit notifications.
;; Toggle with M-x magit-notify-mode

;;; Code:

(require 'magit)
(require 'notifications)

(defun magit-notify (orig-fun &rest args)
  (message "magit called with args %S" args)
  (let ((repo (projectile-project-name))
        (branch (magit-get-current-branch))
        (res (apply orig-fun args)))
    (message "magit returned %S" res)
    (notifications-notify :title (format "magit finished on %s, branch %s" repo branch)
                          :urgency (if (= res 1)
                                       'critical
                                     'normal)
                          :body (format "message: %S" (if (boundp 'magit-this-error)
                                                          magit-this-error
                                                        "mmh no error message ?")))
    res))

;;;###autoload
(define-minor-mode magit-notify-mode
  "Show magit (desktop) notifications."
  :lighter ""
  :global t
  ;; ;TODO: to test
  (if magit-notify-mode
      (progn
        (advice-add 'magit-process-finish :around #'magit-notify))

    (advice-remove 'magit-process-finish #'magit-notify)))

(provide 'magit-notify)
